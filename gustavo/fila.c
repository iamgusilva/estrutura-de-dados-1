#include <stdio.h>
#include "fila.h"

int main(){

  Fila fila;

  criar(&fila);
  inserir(&fila, 8);
  inserir(&fila, 3);
  inserir(&fila, 2);
  inserir(&fila, 1);
  inserir(&fila, 6);
  inserir(&fila, 7);
  inserir(&fila, 0);
  inserir(&fila, 4);
  inserir(&fila, 9);
  inserir(&fila, 5);
  imprimir(&fila);
  remover(&fila);
  imprimir(&fila);

}
