#define TAMANHO 10

typedef struct Fila{
  int fila[TAMANHO];
  int inicio, fim;
}Fila;

void criar(Fila *f){
  f->inicio = 0;
  f->fim = -1;
  printf("Criando fila.");
}

void inserir(Fila *f, int elem){
  f->fim++;
  f->fila[f->fim] = elem;
}

void remover(Fila *f){
  f->inicio++;
}

void imprimir(Fila *f){
  printf("%d",f->fila[f->inicio]);
}
